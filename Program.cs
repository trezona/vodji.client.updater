﻿using System;
using Vodji.Client.Updater.Packs;

namespace Vodji.Client.Updater
{
    class Program
    {
        static void Main(string[] args)
        {
            var packManager = new PackManager();
            {
                packManager.AddPack<CefPack>();
                packManager.CheckupAsync().Wait();
            }
        }
    }
}
