﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using Vodji.Client.Updater.Interfaces;

namespace Vodji.Client.Updater
{
    public class PackManager
    {
        ~PackManager()
        {
            Console.ForegroundColor = ConsoleColor.White;
        }

        public bool AddPack<T>() where T : IPackData
        {
            if (!_packQueueTypes.Contains(typeof(T)))
            {
                _packQueueTypes.Enqueue(typeof(T));
                return true;
            }

            return false;
        }

        public async Task<bool> CheckupAsync(bool installAfter = true)
        {
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(" Checking missing packages");
            Console.WriteLine(" -------------------------");

            var checkupResult = await Task.Factory.StartNew(() =>
            {
                while (_packQueueTypes.TryDequeue(out var packType))
                {
                    var packData = Activator.CreateInstance(packType) as IPackData;
                    if (packData.CheckVersion(out var lastVersion))
                    {
                        Console.ForegroundColor = lastVersion ? ConsoleColor.Green : ConsoleColor.White;
                        Console.WriteLine(" > '{0} ({1})' installed", packData.PackName, packData.GetVersion());
                        if (lastVersion) continue;
                    }

                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(" > '{0} ({1})' not installed", packData.PackName, packData.GetLastVersion());
                    EnqueueInstall(packData);
                }

                Console.ForegroundColor = ConsoleColor.White;
                return _packQueueInstalls.Any();
            });

            if (checkupResult && installAfter)
            {
                checkupResult = await InstallAsync();
            }

            return checkupResult;
        }

        public async Task<bool> InstallAsync()
        {
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(" Install Missing Pack");
            Console.WriteLine(" --------------------");

            var installResult = true;
            while (_packQueueInstalls.TryDequeue(out var packData))
            {
                if (!await InstallAsync(packData))
                {
                    installResult = false;
                }
            }

            if (installResult)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine();
                Console.WriteLine(" --------------------------");
                Console.WriteLine(" All packages are installed");
            }
            else
            {

            }

            return installResult;
        }

        public async Task<bool> InstallAsync(IPackData packData)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(" > '{0} ({1})' Installing the package", packData.PackName, packData.GetLastVersion());

            if(await packData.InstallAsync(LogTrace))
            {
                LogTrace("OK", ConsoleColor.Green);
                return true;
            }

            return default;
        }

        private bool EnqueueFailInstall(IPackData packData)
        {
            if (!_packQueueFailInstalls.Contains(packData))
            {
                _packQueueFailInstalls.Enqueue(packData);
                return true;
            }

            return false;
        }

        private bool EnqueueInstall(IPackData packData)
        {
            if (!_packQueueInstalls.Contains(packData))
            {
                _packQueueInstalls.Enqueue(packData);
                return true;
            }

            return false;
        }

        private void LogTrace(string msg, ConsoleColor color = ConsoleColor.White)
        {
            Console.ForegroundColor = color;
            Console.WriteLine("    - {0}", msg);
        }

        private ConcurrentQueue<IPackData> _packQueueFailInstalls = new ConcurrentQueue<IPackData>();
        private ConcurrentQueue<IPackData> _packQueueInstalls = new ConcurrentQueue<IPackData>();
        private ConcurrentQueue<Type> _packQueueTypes = new ConcurrentQueue<Type>();
    }
}
