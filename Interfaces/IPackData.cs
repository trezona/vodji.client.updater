﻿using System;
using System.Threading.Tasks;

namespace Vodji.Client.Updater.Interfaces
{
    public delegate void LogTrace(string msg, ConsoleColor color = ConsoleColor.White);

    public interface IPackData
    {
        public string PackName { get; }

        public string GetVersion();
        public string GetLastVersion();
        public bool CheckVersion(out bool lastVersion);

        public Task<bool> InstallAsync(LogTrace logTrace);
    }
}
