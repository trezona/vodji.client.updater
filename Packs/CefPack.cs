﻿using System.IO;
using System.Diagnostics;
using Vodji.Client.Updater.Interfaces;
using System.Threading.Tasks;
using System;
using System.Net.Http;
using ICSharpCode.SharpZipLib.Tar;
using ICSharpCode.SharpZipLib.BZip2;
using System.Text;

namespace Vodji.Client.Updater.Packs
{
    public struct CefPack : IPackData
    {
        // Cef Checker
        public const string CefLastVersion = "88.1.6";
        public const string CefLastVersionFile = "88.1.6+g4fe33a1+chromium-88.0.4324.96";
        public const string CefFileName = "libcef.dll";

        static CefPack()
        {
            if (File.Exists(CefFileName))
            {
                var cefInfoFile = FileVersionInfo.GetVersionInfo(CefFileName);
                CefVersionFile = cefInfoFile.ProductVersion;
                CefVersion = string.Format("{0}.{1}.{2}",
                    cefInfoFile.FileMajorPart,
                    cefInfoFile.FileMinorPart,
                    cefInfoFile.FileBuildPart);
            }
        }

        static public string CefVersion { get; }
        static public string CefVersionFile { get; }

        // Proccess Pack
        public string PackName => "Chromium Embedded Framework";

        public bool CheckVersion(out bool lastVersion)
        {
            lastVersion = CefVersion == CefLastVersion;
            return !string.IsNullOrWhiteSpace(CefVersion);
        }

        public string GetVersion() => CefVersion;
        public string GetLastVersion() => CefLastVersion;

        public async Task<bool> InstallAsync(LogTrace logTrace)
        {
            var cefFile = string.Format("cef_binary_{0}_{1}_minimal.tar.bz2", CefLastVersionFile, "windows64");
            var cefUrl = string.Format("https://cef-builds.spotifycdn.com/{0}", cefFile);

            if (!File.Exists(cefFile))
            {
                logTrace("Download File...");
                using var httpClient = new HttpClient();
                try
                {
                    var downloadingBytes = await httpClient.GetByteArrayAsync(cefUrl);
                    using var createdFile = File.Open(cefFile, FileMode.Create);
                    {
                        logTrace("Create File...");
                        createdFile.Write(downloadingBytes, default, downloadingBytes.Length);
                        createdFile.Close();
                    }

                    httpClient.Dispose();
                }
                catch (Exception e)
                {
                    logTrace($"Installation Error ({e.Message})", ConsoleColor.Red);
                }
            }

            logTrace("Unpack File...");
            TarEntry fileInTar;
            string[] unpackDirs = new[] { "Release/", "Resources/" };

            try
            {
                using var cachedFile = File.Open(cefFile, FileMode.Open);
                using (var decompressed = new BZip2InputStream(cachedFile))
                {
                    var tarInput = new TarInputStream(decompressed, Encoding.UTF8);
                    var tempFileName = Path.GetFileNameWithoutExtension(cefFile);
                    {
                        var lastIndexOf = tempFileName.LastIndexOf('.');
                        tempFileName = tempFileName.Substring(0, lastIndexOf);
                    }

                    while ((fileInTar = tarInput.GetNextEntry()) != null)
                    {
                        var tarEntryName = fileInTar.Name.Remove(0, tempFileName.Length + 1);
                        if (string.IsNullOrWhiteSpace(tarEntryName)) continue;

                        foreach (var unpackDir in unpackDirs)
                        {
                            if (fileInTar.IsDirectory) break;

                            var tarEntryDir = Path.GetDirectoryName(tarEntryName);
                            tarEntryName = tarEntryName.Replace(unpackDir, default);
                            if (tarEntryDir.Contains(unpackDir) || string.IsNullOrWhiteSpace(tarEntryDir))
                            {
                                if (File.Exists(tarEntryName)) break;
                                using var newFile = File.Open(tarEntryName, FileMode.Create);
                                {
                                    logTrace(" > Copy: " + tarEntryName);
                                    tarInput.CopyEntryContents(newFile);
                                }
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logTrace($"Installation Error ({ex.Message})", ConsoleColor.Red);
                return false;
            }

            return true;
        }
    }
}
